var arrayPersonajes = [];

var personaje = [];
var random = 0;
var item;

var specieSelected = "";
var specie;
var genderSelected = "";
var gender;
var name = "";

var auxGender;
var auxSpecie;
var auxName;
var filterCounter;
var arrayCounter = [];
var maxValue;
var max;
var band;
var counter;
var i;
//Loading Cards Functions******************************
var numPage = Math.floor(Math.random() * 26)
fetch(`https://rickandmortyapi.com/api/character/?page=${numPage}`)
    .then((res) => res.json())
    .then((data) => {
        loadCharacters(data);
    })


function getRandomItem(data) {
    var random = Math.floor(Math.random() * 20);
    return data.results[random];
}

function isItemInCharacters(item) {
    return personaje.includes(item.id)
}

function getItem(data) {
    var item = getRandomItem(data);

    while (isItemInCharacters(item)) {
        item = getRandomItem(data);
    }
    personaje.push(item.id);

    return item;
}

function drawCard(item) {
    document.getElementById('output').innerHTML += `
         <div class="col-sm-6 col-md-4 col-lg-3 col-xl-3">
             <div class="card" id="card">
                <img class="card-img-top" src="${item.image}">
                    <div class="card-body">
                        <h5 id="title-char" class="card-title">${item.name}</h5>
                        <div id="text-prop">
                            <div id="c1" class="form-row custom-text">
                                <span class="col">STATUS</span>
                                <p class="col attributes">${item.status}</p>
                            </div>
                            <div id="c2" class="form-row custom-text">
                                <span class="col">SPECIES</span>
                                <p class="col attributes">${item.species}</p>
                            </div>
                            <div id="c3" class="form-row custom-text">
                                <span class="col">GENDER</span>
                                <p class="col attributes">${item.gender}</p>
                            </div>
                            <div id="c4" class="form-row custom-text">
                                <span class="col">ORIGIN</span>
                                <p class="col attributes">${item.origin.name}</p>
                            </div>
                            <div id="c5" class="form-row custom-text">
                                <span class="col">LOCATION</span>
                                <p class="col attributes">${item.location.name}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `;
}

function loadCharacters(data) {
    for (var i = 0; i < 4; i++) {
        item = getItem(data);
        arrayPersonajes.push(item);
        drawCard(item);
    }
}

function loadCards() {
    var numPage = Math.floor(Math.random() * 26)
    fetch(`https://rickandmortyapi.com/api/character/?page=${numPage}`)
        .then((res) => res.json())
        .then((data) => {
            loadCharacters(data);
        })
}

//Filtering Functions*********************************
function sendSpecie() {
    specieSelected = document.getElementById("selectSpecie");
    specie = specieSelected.options[specieSelected.selectedIndex].text;
}
function sendGender() {
    genderSelected = document.getElementById("selectGender");
    gender = genderSelected.options[genderSelected.selectedIndex].text;
}
function getNameText() {
    return document.getElementById('characterName').value;
}
function resetFilters() {
    genderSelected.selectedIndex = 0;
    specieSelected.selectedIndex = 0;
    document.getElementById('characterName').value = "";
    gender = undefined;
    specie = undefined;
    name = "";
    arrayCounter=[];
}
function getItemNumber(aux) {
    counter = 0;
    if (name != "") {
        auxName = aux.name.toUpperCase();
        if (auxName.includes(name.toUpperCase())) {
            counter++;
        }
    }
    if (gender != undefined) {
        if (aux.gender == gender) {
            counter++;
        }
    }
    if (specie != undefined) {
        if (aux.species == specie) {
            counter++;
        }
    }
    return counter;
}
function getMaximo(array) {
    max = 0;
    band = true;
    array.forEach(function (num) {
        if (band) {
            max = num;
            band=false;
        } else {
            if (max < num) {
                max = num;
            }
        }
    });
    console.log(max);
    return max;
}
function filterBySelection() {
    name = getNameText();
    maxValue = 0;
    filterCounter = 0;
    if (name != "" || gender != undefined || specie != undefined) {
        document.getElementById('output').innerHTML = ``;
        arrayPersonajes.forEach(function (auxItem) {
            arrayCounter.push(getItemNumber(auxItem));
        });
    
        maxValue = getMaximo(arrayCounter);
    
        i=0;
        arrayPersonajes.forEach(function (auxItem) {
            if (arrayCounter[i]==maxValue) {
                drawCard(auxItem);
            }
            i++;
        });
    }
    resetFilters();
}

function resetSelection() {
    document.getElementById('output').innerHTML = ``;
    arrayPersonajes.forEach(function (auxItem) {
        drawCard(auxItem);
    });
}